import React from 'react';
import styled from 'styled-components';
import LobbyList from '../components/LobbyList';

const Container = styled.div`
  height: 100vh;
  padding: 8px;
`;

function InpexPage() {
  return (
    <Container>
      Ichiban Technical Test
      By Andrew Azzopardi
      <hr />
      <LobbyList />
    </Container>
  );
}

export default InpexPage;
