import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import GameProvider from '../components/GamesProvider';
import theme from '../utils/theme';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }

  * {
    font-family: Inter, Helvetica, sans-serif;
    box-sizing: border-box;
  }
`;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <GameProvider>
          {/* eslint-disable-next-line react/jsx-props-no-spreading */}
          <Component {...pageProps} />
        </GameProvider>
      </ThemeProvider>
    </>
  );
}

export default MyApp;
