import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styled from 'styled-components';
import Arrow from './Arrow';

const Container = styled.div`
  overflow: hidden;
  min-width: 0;
`;

const StyledSlider = styled(Slider)`
  & > .slick-dots {
    bottom: 48px;

    & > li {
      width: 10px;
      height: 10px;
      margin: 0 10px;

      & > button {
        width: 8px;
        height: 8px;
        border-radius: 5px;
        border: 1px solid #C4C4C4;
  
        &::before {
          content: unset;
        }
      }
  
      &.slick-active > button {
        background: #C4C4C4;
      }
    }
  }
`;

interface ICarouselProps {
  children: React.ReactNode;
}

function Carousel({ children }: ICarouselProps) {
  return (
    <Container>
      <StyledSlider
        infinite
        arrows
        speed={500}
        slidesToShow={2}
        slidesToScroll={2}
        nextArrow={<Arrow right />}
        prevArrow={<Arrow />}
        responsive={[
          {
            breakpoint: 1600,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
            },
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            },
          },
        ]}
      >
        {children}
      </StyledSlider>
    </Container>
  );
}

export default Carousel;
