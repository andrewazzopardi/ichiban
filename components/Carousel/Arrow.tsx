import styled from 'styled-components';

interface IArrowProps {
  right?: boolean;
}

const Arrow = styled.div<IArrowProps>`
  position: absolute;
  top: 50%;
  width: 27px;
  height: 44px;
  margin-top: -22px;
  z-index: 10;
  cursor: pointer;
  left: ${(props) => (props.right ? 'auto' : '10px')};
  right: ${(props) => (props.right ? '10px' : 'auto')};
  margin: 0 16px;
  width: 40px;
  height: 40px;
  background: #01062b;
  color: #fff;
  border-radius: 50%;
  top: calc(50% - 20px);

  &:hover {
    background: #41467b;
  }

  &::before {
    font-family: fontello;
    content: '${(props) => (props.right ? '>' : '<')}';
    font-size: 24px;
    position: absolute;
    width: 100%;
    height: 100%;
    right: 0;
    left: 0;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    z-index: 1;
  }
`;

export default Arrow;
