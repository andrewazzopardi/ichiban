import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { api } from '../utils/apis';
import { IGameLobby } from '../utils/types';
import GameLobby from './GameLobby';
import LobbyButton from './LobbyButton';

const Container = styled.div`

`;

const LobbyButtonContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`;

function LobbyList() {
  const [lobbyList, setLobbyList] = useState<IGameLobby[]>([]);
  const [activeLobbyIndex, setActiveLobbyIndex] = useState(0);

  const getData = async () => {
    const result = await api.get('/game/getLobbyListWithGameGroups?languageId=en');
    setLobbyList(result.data);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container>
      <LobbyButtonContainer>
        {lobbyList.sort((a, b) => a.id - b.id).map((lobby, index) => (
          <LobbyButton
            key={lobby.id}
            onClick={() => setActiveLobbyIndex(index)}
            lobby={lobby}
            isActive={activeLobbyIndex === index}
          />
        ))}
      </LobbyButtonContainer>
      {lobbyList[activeLobbyIndex] && <GameLobby gameLobby={lobbyList[activeLobbyIndex]} />}
    </Container>
  );
}

export default LobbyList;
