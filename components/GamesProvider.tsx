import React, { useCallback, useEffect, useState } from 'react';
import { GamesContext } from '../hooks/contexts';
import { api } from '../utils/apis';
import { IGame } from '../utils/types';

interface IGameProviderProps {
  children: React.ReactNode;
}

function GameProvider({ children }: IGameProviderProps) {
  const [gameGroupGames, setGameGroupGames] = useState<Record<string, IGame[]>>({});

  const getData = useCallback(async () => {
    const result = await api.get('/game/getLobbyGameGroupGames?languageId=en');
    const groupedGames = result.data.reduce((prev: Record<string, IGame[]>, game: IGame) => {
      game.gameGroupList.forEach((gameGroup) => {
        if (prev[gameGroup.id]) {
          prev[gameGroup.id].push(game);
        } else {
          prev[gameGroup.id] = [game];
        }
      });
      return prev;
    }, {});
    setGameGroupGames(groupedGames);
  }, []);

  useEffect(() => {
    getData();
  }, [getData]);

  return (
    <GamesContext.Provider value={{ gameGroupGames, setGameGroupGames }}>
      {children}
    </GamesContext.Provider>
  );
}

export default GameProvider;
