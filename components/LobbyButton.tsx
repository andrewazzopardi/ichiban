import React from 'react';
import styled from 'styled-components';
import { IGameLobby } from '../utils/types';

interface IContainerProps {
  isActive: boolean;
}

const Container = styled.div<IContainerProps>`
  width: auto!important;
  border-radius: 50px;
  display: inline-flex;
  align-items: center;
  cursor: pointer;
  padding: 8px 16px 8px 8px;
  font: 700 14px/24px;
  white-space: nowrap;
  margin: 0 8px 8px 0;

  ${(props) => (props.isActive ? `
    background: linear-gradient(
      88.31deg, #7a90f6 .9%,#60a8ff 98.54%
    );
    color: #fff;
  ` : `
    color: #666c87;
    background: #ebedf5;
  `)}
`;

interface ILobbyImageProps {
  imageUrl: string;
}

const LobbyImage = styled.div<ILobbyImageProps>`
  background: white;
  border-radius: 50%;
  width: 32px;
  height: 32px;
  margin-right: 12px;
  position: relative;
`;

interface ILobbyButtonProps {
  onClick: () => void;
  isActive: boolean;
  lobby: IGameLobby;
}

function LobbyButton({ lobby, isActive, onClick }: ILobbyButtonProps) {
  return (
    <Container isActive={isActive} onClick={onClick}>
      <LobbyImage imageUrl="" />
      {lobby.name}
    </Container>
  );
}

export default LobbyButton;
