import React from 'react';
import styled from 'styled-components';
import { useGames } from '../hooks/contexts';
import { IGameGroup } from '../utils/types';
import Carousel from './Carousel/Carousel';
import Game from './Game';

const Container = styled.div`
  display: grid;
  grid-template-areas:
    'title viewAll'
    'games games';
  justify-content: space-between;
`;

const Title = styled.div`
  grid-area: title;
  color: #01062b;
  font: 700 16px/20px ;
  text-align: left;
`;

const ViewAll = styled.div`
  grid-area: viewAll;    
  font: 700 16px/20px;
  text-align: right;
  color: #4277ff;
  cursor: pointer;
`;

const GamesContainer = styled.div`
  grid-area: games;
  width: 100%;
  min-width: 0;
`;

interface IGameGroupProps {
  gameGroup: IGameGroup;
}

function GameGroup({ gameGroup }: IGameGroupProps) {
  const { gameGroupGames } = useGames();

  return (
    <Container>
      <Title>{gameGroup.name}</Title>
      <ViewAll>View All</ViewAll>
      <GamesContainer>
        <Carousel>
          {gameGroupGames[gameGroup.id]?.map((game) => (
            <Game key={game.name} game={game} />
          ))}
        </Carousel>
      </GamesContainer>
    </Container>
  );
}

export default GameGroup;
