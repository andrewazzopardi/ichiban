import React from 'react';
import styled from 'styled-components';
import { IGame } from '../utils/types';

const Container = styled.div`
  margin-right: 16px;
`;

const ImageContainer = styled.div`
  padding: calc(100% / (263 / 329)) 0 0;
  background: lightblue;
`;

const Title = styled.div`
  font-size: 14px;
  line-height: 20px;
  color: #666c87;
`;

interface IGameProps {
  game: IGame;
}

function Game({ game }: IGameProps) {
  return (
    <Container>
      <ImageContainer />
      <Title>
        {game.name}
      </Title>
    </Container>
  );
}

export default Game;
