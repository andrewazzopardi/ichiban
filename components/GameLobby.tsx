import React from 'react';
import { IGameLobby } from '../utils/types';
import GameGroup from './GameGroup';

interface IGameLobbyProps {
  gameLobby: IGameLobby;
}

function GameLobby({ gameLobby }: IGameLobbyProps) {
  return (
    <div>
      {gameLobby.gameGroupList
        .sort((a, b) => a.game_group_order - b.game_group_order)
        .map((gameGroup) => (
          <GameGroup key={gameGroup.id} gameGroup={gameGroup} />
        ))}
    </div>
  );
}

export default GameLobby;
