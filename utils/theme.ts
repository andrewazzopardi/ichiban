const theme = {
  breakpoints: {
    sm: '320px',
    md: '640px',
  },
} as const;

export default theme;
export type TTheme = typeof theme;
