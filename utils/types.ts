/* eslint-disable camelcase */
export interface IGameGroup {
  id: number;
  name: string;
  group_type: string;
  group_sub_type: string;
  game_asset_style: string;
  game_group_order: number;
  content: string;
  metaTags: string;
  metaDescription: string;
}

export interface IGameLobby {
  id: number;
  name: string;
  user_segment: string;
  lobby_order: number;
  content: string;
  metaTags: string;
  metaDescription: string;
  gameGroupList: IGameGroup[];
}

export interface IGame {
  gameCode: string;
  aspectRatio: number;
  aspectRationValue: string;
  hasDemo: 1 | 0;
  name: string;
  externalAssets: string;
  gdContent: string;
  metaTags: string;
  metaDescription: string;
  vendorDisplayName: string;
  vendorCode: string;
  tagList: string[];
  displayOrder: number;
  gameType: string;
  beGameTypeId: number;
  vendorName: string;
  pristineGameId: string;
  vendorSlug: string;
  gameGroupList: IGameGroup[];
}
