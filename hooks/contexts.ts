import { createContext, useContext } from 'react';
import { IGame } from '../utils/types';

export interface IGamesContext {
  gameGroupGames: Record<string, IGame[]>;
  setGameGroupGames: (newValue: Record<string, IGame[]>) => void;
}

const GamesContext = createContext<IGamesContext>({
  gameGroupGames: {},
  setGameGroupGames: () => { },
});

const useGames = () => useContext(GamesContext);

export {
  GamesContext,
  useGames,
};
